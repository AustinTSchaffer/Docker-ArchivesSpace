FROM archivesspace/archivesspace:2.5.1

LABEL author="Austin Schaffer <schaffer.austin.t@gmail.com>"

COPY docker-startup.sh /archivesspace/startup.sh
RUN chmod u+x /archivesspace/startup.sh

CMD ["/archivesspace/startup.sh"]
