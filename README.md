# Docker ArchivesSpace

Docker image inheriting from the `archivesspace/archivesspace` series of Docker
images, containing a few minor tweaks that allow the image to start up faster.

The idea behind this Docker image is to decrease the startup time for an
ArchivesSpace Docker container, by omitting resources that are useful for a
production ArchivesSpace server. This allows the ArchivesSpace Container to
focus on starting ArchivesSpace, without being required to install things like
SendMail.

## About

This image inherits from the `archivesspace/archivesspace` series of Docker
images. 

- The `/startup.sh` can process a few new environment variables, mostly to
  prevent the archivesspace service from starting until the MySQL instance is
  reachable and responsive.
- You no longer need to specify the `APPCONFIG_DB_URL` environment variable. The
  docker-startup script will build the variable from the new environment
  vairables.
- The `/startup.sh` no longer installs SendMail.

Other than these differences, the container should behave exactly the same
as the `archivesspace/archivesspace` image that has the same name.

## New Environment Variables

This image creates an ArchivesSpace container that will wait until the specified
MySQL instance is available.

**DB_ADDR**: Location where the MySQL instance exists. Used to supply the
`--host` argument on the mysql command. Defaults to `"db"`, which is commonly
used as the service name for databases when using docker-compose.

**MYSQL_PORT**: Defaults to `3306`.

**MYSQL_USER**: The script will attempt to run a `SHOW DATABASES;` on the
instance using this user to see if the MySQL instance is up and running.
Defaults to `"as"`.

**MYSQL_PASSWORD**: The password for the user account. Defaults to `"as123"`.

**MYSQL_DELAY**: Specified the approximate maximum number of
seconds that the ArchivesSpace container will wait, specified as a quantity of
seconds. If the counter reaches this number before the MySQL instance is
available, then the startup script will exit and the container will go offline.
Defaults to `60`.

**MYSQL_CHECK_INTERVAL**: Specifies the number of seconds in
between each ping. Defaults to `5`.


## Usage

```bash
docker run -d -p 8089:8089 austintschaffer/archivesspace:2.4.1

# It takes about a minute to spin up, then:
curl http://localhost:8089
```

Example for using docker-compose with the `austintschaffer/aspace-db:2.4.1`
Docker image, which contains a pre-loaded ArchivesSpace database. Replace the
`db` service with however you prefer to manage your ArchivesSpace database. All
environment variables have been explicitly specified in this sample, but not all
have to be when running this yourself.

```yml
version: '2'
services:
  db:
    image: austintschaffer/aspace-db:2.5.1
    command: --character-set-server=utf8 --collation-server=utf8_unicode_ci --innodb_buffer_pool_size=2G --innodb_buffer_pool_instances=2 --lower_case_table_names=1
    environment:
      MYSQL_ROOT_PASSWORD: 123456
      # Include these so the CREATE USER statements are executed. 
      # Alternatively, add some `volume:` statements for multiple and custom 
      # CREATE USER statements
      MYSQL_DATABASE: archivesspace
      MYSQL_USER: as
      MYSQL_PASSWORD: as123
    ports:
      - 3306:3306

  aspace:
    image: archivesspace/archivesspace:2.5.1
    environment:
      DB_ADDR: db
      MYSQL_PORT: 3306
      MYSQL_USER: as
      MYSQL_PASSWORD: as123
      MYSQL_DELAY: 240
      MYSQL_CHECK_INTERVAL: 3
    depends_on:
      - db
    ports:
      - 8080:8080
      - 8081:8081
      - 8082:8082
      - 8089:8089
      - 8090:8090

```

